Cableado/Conexión vía Ethernet
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note::

   Si conoce cómo se realiza la conexión física de la comunicación vía RS-485 entre el PLC y 
   la PC, puede saltear esta etapa, y dirigirse directamente a la sección :ref:`ethernetConfiguration`.