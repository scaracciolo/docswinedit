Comunicación vía RS-485
-----------------------

.. toctree::
   :maxdepth: -1
   :numbered:

   Cableado/Conexión física <rs485Conection>
   Configuración de la comunicación en WinEdit <rs485Configuration>
