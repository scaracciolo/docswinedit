SUB
---
 
.. sidebar:: Símbolos LD

   .. figure:: SUBsym_1.png
      :align: center
   
   *A partir de ROM 100 en Ladder*

   .. figure:: SUBsym_2.png
      :align: center

   *A partir de ROM 260 en Ladder*

**Descripción:** Computa la resta entre dos variables.

**Funcionamiento:** 

   * Con un único parámetro de entrada: Computa la resta entre el acumulador ``Acc`` y el ``Parámetro 1`` **incondicionalmente**. El resultado es copiado en el acumulador ``Acc``.
   
   * Con dos parámetros de entrada: Computa la resta entre el ``Parámetro 1`` y el ``Parámetro 2`` **condicionalmente**. El resultado es copiado en el acumulador aritmético ``AAcc``.

|
|

.. csv-table::
   :header: "Parámetro", "Tipo", "Descripción"

   1, Entrada, Variable con el primer término de la resta **[1]**.
   2, Entrada, Variable con el segundo término de la resta.

**[1]** En el caso de un solo parámetro es el segundo término.

**Ejemplo LD con un parámetro de entrada**

.. figure:: ej1_SUB_1param.png
   :align: center

.. figure:: ej1_SUB_1param_mmap.png
   :align: center

**Ejemplo IL un parámetro de entrada**

.. code::

   LD Variable
   SUB Parametro1
   ST Resultado

**Ejemplo LD con dos parámetros de entrada**

.. figure:: ej1_SUB_2param.png
   :align: center

.. figure:: ej1_SUB_2param_mmap.png
   :align: center

**Ejemplo IL dos parámetros de entradas**

.. code::

   LD Habilitacion
   SUB Parametro1, Parametro2
   CMOV AAcc, Resultado