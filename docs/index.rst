.. wineditdocs documentation master file, created by
   sphinx-quickstart on Wed Feb  7 12:43:34 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenido a la documentación de WinEdit!
#########################################

..
      Vamos *viendo* si funciona, **rato**.
      La idea sería ver qué pasa con los enters.

      Y además acá va una nota

      .. note::

      Esta es una nota moy uimiportante gente. Ojo.

      .. warning::

      veámos qué pasa con un Warning!

      .. versionadded:: 1.0
      Acá se pueden aclarar nuevas cosillas de una versión en particular

      .. seealso::

      Module :py:mod:`zipfile`
            Documentation of the :py:mod:`zipfile` standard module.
      `GNU tar manual, Basic Tar Format <http://link>`_
            Documentation for tar archive files, including GNU tar extensions.

      .. hlist::
      :columns: 3

      * A list of
      * short items
      * that should be
      * displayed
      * horizontally

      .. glossary::

      environment
            A structure where information about all documents under the root is saved, and used 
            for cross-referencing. The environment is pickled fter the parsing stage, so that successive 
            runs only need to read and parse new and changed documents.

      source directory
            The directory which, including its subdirectories, contains all source files for one Sphinx project.

.. toctree::
   :maxdepth: -1
   :numbered:
   :caption: Índice de contenidos

   Guía rápida - Primeros pasos <getStarted>
   Tipo de datos del PLC <dataType>
   Set de instrucciones <instructionSet>
   Guía de comunicación <comunicationGuide>
   Manual de programación <manual>