.. _PCPort:

¿Cúal es el nombre del puerto de la PC conectado al PLC?
--------------------------------------------------------

Para conocer a qué puerto de la PC se encuentra conectado el dispositivo, puede seguir los siguientes 
pasos en el *Escritorio de Windows*:

1. *Click* derecho en ``Equipo`` (Mi PC).
2. *Click* izquierdo en ``Administrar``.
3. Desplegar el menú ``Administrador del sistema`` en el panel de la izquierda.
4. *Click* izquierdo en la opción ``Administrador de dispositivos``.
5. *Click* izquierdo en la sección ``Puertos (COM & LPT)`` del menú central.

Llegado a este punto, el usuario encontrará una lista de puertos establecidos en la PC como se 
muestra en la siguiente captura de pantalla.

.. image:: _static/COMPorts.png
      :align: center
