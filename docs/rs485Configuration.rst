.. _rs485Configuration:

Establecer conexión vía RS-485
------------------------------

Para establecer la comunicación del PLC con la PC, deberá configurar una serie de parámetros desde *WinEdit* 
que serán explicados a continuación:

Primero, debe dirigirse a ``PLC`` -> ``Interrogar / Propiedades``. 

.. image:: _static/paso3.png
      :align: center

Luego, en la siguiente pantalla, deberá definir las siguientes opciones:

.. image:: _static/paso4.png
      :align: center

1. **Buscar por broadcast (ID = 0):** Al seleccionar esta opción nos independizamos de conocer el *ID* del 
equipo para comunicarnos. Esta opción interroga a todos los equipos de la red para conocer qué dispositivos 
se encuentran disponibles.

.. note::

   Si conoce el *ID* del equipo al que quiere comunicarse pueder seleccionar **Buscar por barrido de ID desde** 
   e indicar el valor conocido.

2. **Velocidades a usar:** Define la velocidad que se usará para establecer la comunicación. La elección de 
cada opción depende de la configuración del puerto del PLC.

.. note::

   Todos lo equipos **CAIPE** tienen configurado ``9600 baudios`` cuando salen de fábrica. 

3. **COMx:** Es el nombre del puerto de la **PC** al que está conectado el PLC. Si desconoce el nombre 
del puerto, puede dirigirse a un pequeño explicativo de cómo conocerlo en la sección :ref:`PCPort`.

.. warning::

   No confundir el nombre del puerto de la **PC** con el nombre del puerto del **PLC** que se encuentra 
   en la serigrafía del mismo.

4. **Usar Ethernet:** No tildar para establecer modo *RS-485*.

Posteriomente, debe hacer *click* en ``Buscar`` y obtendrá la respuesta del PLC:

.. image:: _static/paso5.png
      :align: center

Como puede observar en la figura, la respuesta del PLC contiene la información solicitada al momento de 
la creación del proyecto (:ref:`newProyect`):

* ID = 1
* Modelo: CP121
* Programar con ROM: 262 

Al hacer *click* en ``a ROM`` son cargados estos valores en las *Propiedades del proyecto* lo que permiritá 
la correcta comunicación del equipo cuando deseemos descargar o cargar programas al PLC e incluso 
monitorearlo.

Finalmente, hay que configurar en el *panel inferior* el puerto a utilizar, cuyo número es el mismo que 
ha sido establecido en el paso anterior.

.. image:: _static/paso6.png
      :align: center