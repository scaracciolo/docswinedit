Aritméticas
===========

.. toctree::
   :maxdepth: -1

   Módulo <instructions/ABS>
   Suma <instructions/ADD>
   División <instructions/DIV>
   Resto <instructions/MOD>
   Multiplicacion <instructions/MUL>
   Resta <instructions/SUB>
