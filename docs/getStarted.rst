Guía rápida - Primeros pasos
****************************

La *Guía rápida* para comenzar a utilizar **WinEdit** pretende ayudar al usuario con la creación de un
proyecto de programación desde *cero*, involucrando todos los pasos necesarios para descargalo en el PLC. 
Para ello, se listarán a continuación una serie de pasos sencillos y prácticos que permitirán al usuario
incursionar rápidamente con el *software* en cuestión:

.. toctree::
   :maxdepth: -1

   Nuevo proyecto <newProyect>
   Establecer comunicación con el PLC <plcComunication>