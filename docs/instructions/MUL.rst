MUL
---
 
.. sidebar:: Símbolos LD

   .. figure:: MULsym_1.png
      :align: center
   
   *A partir de ROM 100 en Ladder*

   .. figure:: MULsym_2.png
      :align: center

   *A partir de ROM 260 en Ladder*

**Descripción:** Computa la multiplicación entre dos variables.

**Funcionamiento:** 

   * Con un único parámetro de entrada: Computa la multiplicación entre el acumulador ``Acc`` y el ``Parámetro 1`` **incondicionalmente**. El resultado es copiado en el acumulador ``Acc``.
   
   * Con dos parámetros de entrada: Computa la multiplicación entre el ``Parámetro 1`` y el ``Parámetro 2`` **condicionalmente**. El resultado es copiado en el acumulador aritmético ``AAcc``.

|
|

.. csv-table::
   :header: "Parámetro", "Tipo", "Descripción"

   1, Entrada, Variable con primer número de la multiplicación.
   2, Entrada, Variable con segundo número de la multiplicación.

**Ejemplo LD con un parámetro de entrada**

.. figure:: ej1_MUL_1param.png
   :align: center

.. figure:: ej1_MUL_1param_mmap.png
   :align: center

**Ejemplo IL un parámetro de entrada**

.. code::

   LD Variable
   MUL Parametro1
   ST Resultado

**Ejemplo LD con dos parámetros de entrada**

.. figure:: ej1_MUL_2param.png
   :align: center

.. figure:: ej1_MUL_2param_mmap.png
   :align: center

**Ejemplo IL dos parámetros de entradas**

.. code::

   LD Habilitacion
   MUL Parametro1, Parametro2
   CMOV AAcc, Resultado