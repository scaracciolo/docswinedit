DIV
---
 
.. sidebar:: Símbolos LD

   .. figure:: DIVsym_1.png
      :align: center
   
   *A partir de ROM 100 en Ladder*

   .. figure:: DIVsym_2.png
      :align: center

   *A partir de ROM 260 en Ladder*

**Descripción:** Computa división entre dos variables.

**Funcionamiento:** 

   * Con un único parámetro de entrada: Computa la división entre el acumulador ``Acc`` y el ``Parámetro 1`` **incondicionalmente**. El resultado es copiado en el acumulador ``Acc``.
   
   * Con dos parámetros de entrada: Computa la división entre el ``Parámetro 1`` y el ``Parámetro 2`` **condicionalmente**. El resultado es copiado en el acumulador aritmético ``AAcc``.

|
|

.. csv-table::
   :header: "Parámetro", "Tipo", "Descripción"

   1, Entrada, Variable con numerador **[1]**.
   2, Entrada, Variable con denominador.

**[1]** Para el caso de un solo parámetro corresponde al denominador.

**Ejemplo LD con un parámetro de entrada**

.. figure:: ej1_DIV_1param.png
   :align: center

.. figure:: ej1_DIV_1param_mmap.png
   :align: center

**Ejemplo IL un parámetro de entrada**

.. code::

   LD Variable
   DIV Parametro1
   ST Resultado

**Ejemplo LD con dos parámetros de entrada**

.. figure:: ej1_DIV_2param.png
   :align: center

.. figure:: ej1_DIV_2param_mmap.png
   :align: center

**Ejemplo IL dos parámetros de entradas**

.. code::

   LD Habilitacion
   DIV Parametro1, Parametro2
   CMOV AAcc, Resultado