.. _newProyect:

Nuevo proyecto
==============

Inicialmente, debe realizar con el *mouse* la secuencia ``Archivo`` -> ``Nuevo``.

.. image:: _static/paso1.png
      :align: center

En la siguiente ventana debe cargar las *propiedades del proyecto* y luego hacer *click* en ``Aceptar``. 

.. image:: _static/paso2.png
      :align: center

A continuación se explica cada campo requerido:

1. **Nombre:** Nombre del proyecto que requiera el usuario.
2. **Ladder:** Si está marcado la rutina principal se programará en lenguaje *Ladder* (LD), caso contrario, en *lista de instrucciones* (IL). 
3. **Nº ID:** Indica el número de identificación *Modbus* del PLC a conectar.

.. note::

   Los equipos tienen ``ID = 1`` cuando salen de fábrica.

4. **Guardar todo en un solo archivo:** En caso de no seleccionarlo se crearán varias carpetas con archivos 
asociados al proyecto.

5. **Carpeta de proyecto:** Destino donde se guardará el proyecto.

.. note::

   Haga *click* en ``...`` para navegar las carpetas de *Windows* y buscar la carpeta destino.

6. **Tipo CPU:** Define el equipo a programar.

.. note:: 

   En la serigrafía del equipo puede encontrar el modelo.

7. **ROM:** Establece la versión de la memoria ROM que tiene el equipo.

.. note:: 

   En la serigrafía del equipo puede encontrar una etiqueta con el número de ROM.

.. warning::

   En caso de no conocer ``ID``, ``ROM`` y/o ``CPU`` puede dejar cualquier valor y en la sección  
   :ref:`rs485Configuration` se determinarán automáticamente estos valores.
