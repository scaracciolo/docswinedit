Cableado/Conexión vía RS-485
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Si conoce cómo se realiza la conexión física de la comunicación vía RS-485 entre el PLC y 
la PC, puede saltear esta etapa, y dirigirse directamente a la sección :ref:`rs485Configuration`.

Inicialmente, debemos establecer la conexión física de los equipos para realizar la comunicación. Para 
lograrlo, necesitamos:

1. **Energizar** al PLC con la tensión requerida según su hoja de datos. 

.. note:: 
   
   Para una rápida verificación de la tensión de alimentación requerida puede 
   `ver la tabla de especificaciones <http://caipe.com.ar/productos.php?tabla=plcs_plcs.csv>`_ 
   de los PLCs **CAIPE**.

.. seealso::

   Si necesita **adquirir** una fuente puede `ver la lista de fuentes de alimentación <http://caipe.com.ar/productos.php?tabla=fuentes_de_alimentacion_fuentes_de_alimentacion.csv>`_.

2. **Conectar** un **conversor RS-485 a USB** entre el PLC y la PC. Para ello, debe conectar los terminales 
``A``, ``B`` y ``GND`` desde el **PLC** al **conversor** respetando la nomenclatura en la serigrafía de 
ambos equipos. Luego, el terminar USB que sale del **conversor** se conecta en la PC.

.. warning:: 

   El PLC **CAIPE** puede contar con 1 o más puertos RS-485. Para conocer cúal de ellos se puede utilizar 
   para programar el equipo, debe verificarlo en la hoja de datos correspondiente. Normalmente, el ``COM1`` 
   siempre admite programación.

.. seealso::

   Si necesita **adquirir** un conversor puede `ver la lista de conversores <http://caipe.com.ar/productos.php?tabla=conversores_conversores.csv>`_.