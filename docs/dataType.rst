Variables y memoria del PLC
***************************

Tipos de dato
=============

Los PLCs **CAIPE** cuentan con los siguientes tipos de datos en su memoria:

.. csv-table::
   :header: Nemónico, Punto, Tipo de dato, Tamaño, Registros, Rango

   BOOL, Fijo, Digital, 1 bit, 1/16, 0 a 1
   INT_16, Fijo, Entergo con signo, 16 bit, 1, -32768 a 32676
   INT_32, Fijo, Entero doble con signo, 32 bit, 2, -2147483648 a 2147483647
   REAL, Flotante, Real con signo, 32 bit, 2, -3.4028235·10 :sup:`38` a 3.4028235·10 :sup:`38` [#f1]_

.. warning:: 

   Los tipos de datos que utilizan 2 registros como ``INT_32`` y ``REAL``, siempre son definidos 
   con registros **consecutivos**.

Áreas de memoria
================

Las áreas de memoria pueden tener distintos tipos de datos. En el siguiente cuadro, se muestra la relación 
entre el *tipo de dato* y el *área de memoria*:

.. csv-table::
   :header: Nemónico, Nombre, R/W, BOOL, INT_16, INT_32, REAL

   **I**, Entradas digitales, R, ✓ , ✗ , ✗ , ✗
   **AI**, Entradas analógicas, R, ✗ , ✓ , ✗ , ✗
   **Q**, Salidas digitales, R, ✓ , ✗ , ✗ , ✗
   **AQ**, Salidas analógicas, R, ✗ , ✓ , ✗ , ✗
   **V**, Variables, R/W, ✓ , ✓ , ✓ , ✓
   **M**, Variables c/retención, R/W, ✓ , ✓ , ✓ , ✓
   **SM**, Variables del sistema, R/W [#f2]_, ✓ , ✓ , ✓ , ✓
   **TP**, Temporizadores, R/W, ✗ , ✓ , ✗ , ✗
   **CT**, Contadores, R/W, ✗ , ✓ , ✗ , ✗

.. rubric::

.. [#f1] El rango interno -1.401298·10 :sup:`−45` a 1.401298·10 :sup:`−45` no se encuentra disponible.
.. [#f2] No todas las variables del sistema pueden escribirse.

   