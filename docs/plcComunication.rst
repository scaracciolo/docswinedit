Establecer comunicación con el PLC
==================================

Antes de comenzar a escribir la lógica del PLC en lenguaje *Ladder*, se procederá a establecer la 
comunicación con el equipo para dejar preparado el terreno para descargar el programa 
sobre el equipo y permitir su posterior monitoreo.

Existen dos formas para comunicarse con los equipos **CAIPE**, un mecanismo es mediante una red 
denominada ``RS-485``, y la otra, ``Ethernet``.

Para leer el tutorial correspondiente, debe elegir a continuación el mecanismo que usará en su sistema:

.. toctree::
   :maxdepth: -1

   Establecer conexión vía RS-485 <rs485Configuration>
   Establecer conexión vía Ethernet <ethernetConfiguration>



