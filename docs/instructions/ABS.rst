ABS
---

.. sidebar:: Símbilo LD

   .. figure:: ABSsym.png
      :align: center
      
      *A partir de ROM 265 en Ladder*

**Descripción:** Computa el valor absoluto de una variable.

**Funcionamiento:** En el ``Parámetro 2`` es copiado el valor absoluto del ``Parámetro 1`` **condicionalmente**.

|
|
|
|
|
|

.. csv-table::
   :header: "Parámetro", "Tipo", "Descripción"

   1, Entrada, Variable con el número al que se computará el módulo.
   2, Salida, Variable donde se copiará el resultado.

**Ejemplo LD**

.. figure:: ej1_ABS.png
   :align: center

.. figure:: ej1_ABS_mmap.png
   :align: center

**Ejemplo IL**

.. code::

   LD Habilitacion
   ABS Parametro1, Parametro2
