ADD
===
 
.. sidebar:: Símbolos LD

   .. figure:: ADDsym_1.png
      :align: center
   
   *A partir de ROM 100 en Ladder*

   .. figure:: ADDsym_2.png
      :align: center

   *A partir de ROM 260 en Ladder*

**Descripción:** Computa la suma entre dos variables.

**Funcionamiento:** 

   * Con un único parámetro de entrada: Computa la suma entre el ``Parámetro 1`` y el acumulador ``Acc`` **incondicionalmente**. El resultado es copiado en el acumulador ``Acc``.
   
   * Con dos parámetros de entrada: Computa la suma entre el ``Parámetro 1`` y el ``Parámetro 2`` **condicionalmente**. El resultado es copiado en el acumulador aritmético ``AAcc``.

|
|

.. csv-table::
   :header: "Parámetro", "Tipo", "Descripción"

   1, Entrada, Variable con un sumando.
   2, Entrada, Variable con otro sumando.

**Ejemplo LD con un parámetro de entrada**

.. figure:: ej1_ADD_1param.png
   :align: center

.. figure:: ej1_ADD_1param_mmap.png
   :align: center

**Ejemplo IL un parámetro de entrada**

.. code::

   LD Variable
   ADD Parametro1
   ST Resultado

**Ejemplo LD con dos parámetros de entrada**

.. figure:: ej1_ADD_2param.png
   :align: center

.. figure:: ej1_ADD_2param_mmap.png
   :align: center

**Ejemplo IL dos parámetros de entradas**

.. code::

   LD Habilitacion
   ADD Parametro1, Parametro2
   CMOV AAcc, Resultado