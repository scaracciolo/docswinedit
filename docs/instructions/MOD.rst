MOD
---
 
.. sidebar:: Símbolos LD

   .. figure:: MODsym_1.png
      :align: center
   
   *A partir de ROM 100 en Ladder*

   .. figure:: MODsym_2.png
      :align: center

   *A partir de ROM 260 en Ladder*

**Descripción:** Computa el resto de la división de dos variables.

**Funcionamiento:** 

   * Con un único parámetro de entrada: Computa el resto de la división del acumulador ``Acc`` y el ``Parámetro 1`` **incondicionalmente**. El resultado es copiado en el acumulador ``Acc``.
   
   * Con dos parámetros de entrada: Computa el resto de la división entre el ``Parámetro 1`` y el ``Parámetro 2`` **condicionalmente**. El resultado es copiado en el acumulador aritmético ``AAcc``.

|
|

.. csv-table::
   :header: "Parámetro", "Tipo", "Descripción"

   1, Entrada, Variable con el numerador **[1]**.
   2, Entrada, Variable con el denominador.

**[1]** En el caso de un solo parámetro es denominador.

**Ejemplo LD con un parámetro de entrada**

.. figure:: ej1_MOD_1param.png
   :align: center

.. figure:: ej1_MOD_1param_mmap.png
   :align: center

**Ejemplo IL con un parámetro de entrada**

.. code::

   LD Variable
   MOD Parametro1
   ST Resultado

**Ejemplo LD con dos parámetros de entrada**

.. figure:: ej1_MOD_2param.png
   :align: center

.. figure:: ej1_MOD_2param_mmap.png
   :align: center

**Ejemplo IL con dos parámetros de entradas**

.. code::

   LD Habilitacion
   MOD Parametro1, Parametro2
   CMOV AAcc, Resultado